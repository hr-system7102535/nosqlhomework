﻿using Microsoft.Extensions.DependencyInjection;

using MongoDB.Driver;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataNoSql
{
    public static class DI
    {
        public static void AddMongoDI(IServiceCollection service)
        {
            service.AddSingleton(new MongoClient("mongodb://localhost:27017"));
        }
    }
}
