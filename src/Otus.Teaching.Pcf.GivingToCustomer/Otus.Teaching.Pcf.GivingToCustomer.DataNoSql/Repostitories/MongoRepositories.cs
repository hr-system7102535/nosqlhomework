﻿using System.Linq.Expressions;

using MongoDB.Bson;
using MongoDB.Driver;

using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataNoSql.Repostitories
{
    public class MongoRepositories<T>
        : IRepository<T>
        where T : BaseEntity
    {
        private readonly IMongoCollection<T> collection;
        public MongoRepositories(MongoClient client)
        {
            this.collection = client.GetDatabase("givingtocustomer").GetCollection<T>(typeof(T).Name);
        }
        public async Task AddAsync(T entity)
        {
            await this.collection.InsertOneAsync(entity);
        }

        public async Task DeleteAsync(T entity)
        {
            await this.collection.FindOneAndDeleteAsync(x => x.Id == entity.Id);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await this.collection.Find(new BsonDocument()).ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await this.collection.FindSync(x=>x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            return await this.collection.FindSync(predicate).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            return await this.collection.Find(x => ids.Contains(x.Id)).ToListAsync();
        }

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            return await this.collection.FindSync(predicate).ToListAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            await this.collection.FindOneAndReplaceAsync(x=> x.Id == entity.Id, entity);
        }
    }
}
