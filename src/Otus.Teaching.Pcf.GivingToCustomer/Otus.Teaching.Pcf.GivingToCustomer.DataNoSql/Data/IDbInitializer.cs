﻿namespace Otus.Teaching.Pcf.GivingToCustomer.DataNoSql.Data
{
    public interface INoSQLInitializer
    {
        void InitializeDb();
    }
}