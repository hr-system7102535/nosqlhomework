﻿using MongoDB.Driver;

using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataNoSql.Data
{
    public class NoSQLInitializer
        : INoSQLInitializer
    {
        private readonly IMongoDatabase database;

        public NoSQLInitializer(MongoClient client)
        {
            database = client.GetDatabase("givingtocustomer");
        }
        
        public void InitializeDb()
        {
            this.database.GetCollection<Preference>("Preference").DeleteMany(x => true);
            this.database.GetCollection<Preference>("Preference").InsertMany(FakeDataFactory.Preferences);            
            this.database.GetCollection<Customer>("Customer").DeleteMany(x => true);
            this.database.GetCollection<Customer>("Customer").InsertMany(FakeDataFactory.Customers);         
        }
    }
}